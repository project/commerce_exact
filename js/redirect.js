/**
 * @file
 * Defines behaviors for the payment redirect form.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the commerceExactRewriteLineItems behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the commerceExactRewriteLineItems behavior.
   */
  Drupal.behaviors.commerceExactRedirect = {
    attach: function (context) {
      // Change all x_line_item_# to x_line_item
      // See https://support.e-xact.com/hc/en-us/articles/360000632374-Hosted-Checkout-Integration-Manual#11.4
      $('.payment-redirect-form', context).find('input[name^="x_line_item"]').attr('name', 'x_line_item');
    }
  };

})(jQuery, Drupal, drupalSettings);
