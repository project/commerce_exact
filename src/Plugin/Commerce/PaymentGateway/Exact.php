<?php

namespace Drupal\commerce_exact\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CommerceGuys\AuthNet\DataTypes\Tax;

/**
 * Provides the E-xact payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "exact_paymentpage",
 *   label = @Translation("E-xact (Payment Page)"),
 *   display_label = @Translation("E-xact"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_exact\PluginForm\ExactPaymentPageForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class Exact extends OffsitePaymentGatewayBase {

  /**
   * E-xact sha1 encryption status.
   */
  const EXACT_ENCRYPTION_SHA1 = 'sha1';

  /**
   * E-xact sha1 encryption status.
   */
  const EXACT_ENCRYPTION_MD5 = 'md5';

  /**
   * E-xact approved status.
   */
  const EXACT_APPROVED = 1;

  /**
   * Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\commerce_exact\Plugin\Commerce\PaymentGateway\Exact exact */
    $exact = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $exact->setLock($container->get('lock'));
    return $exact;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'paymentpage_id' => '',
      'encryption_type' => static::EXACT_ENCRYPTION_SHA1,
      'transaction_key' => '',
      'response_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['paymentpage_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment Page ID'),
      '#description' => $this->t('This is the Payment Page ID from the E-xact Payment Pages.'),
      '#default_value' => $this->configuration['paymentpage_id'],
      '#required' => TRUE,
    ];

    $form['encryption_type'] = [
      '#title' => $this->t('Encryption Type'),
      '#default_value' => $this->configuration['encryption_type'],
      '#type' => 'radios',
      '#options' => [
        static::EXACT_ENCRYPTION_SHA1 => $this->t('HMAC-SHA1'),
        static::EXACT_ENCRYPTION_MD5 => $this->t('HMAC-MD5'),
      ],
      '#description' => $this->t('The default encryption algorithm is HMAC-SHA1. HMAC-MD5 is only advised for legacy implementations.'),
      '#required' => TRUE,
    ];

    $form['transaction_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trasaction key'),
      '#description' => $this->t('The Transaction Key is used in the payment page linking/request.'),
      '#default_value' => $this->configuration['transaction_key'],
      '#required' => TRUE,
    ];

    $form['response_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Response key'),
      '#description' => $this->t('The Response Key is used in sending the payment response back to your website (Relay Response, Silent Post, Redirect)'),
      '#default_value' => $this->configuration['response_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);

    $this->configuration['paymentpage_id'] = $values['paymentpage_id'];
    $this->configuration['encryption_type'] = $values['encryption_type'];
    $this->configuration['transaction_key'] = $values['transaction_key'];
    $this->configuration['response_key'] = $values['response_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
    
    // Do not process the notification if the payment is processing.
    $lock_id = 'commerce_exact_process_payment_'.$order->id();

    if(!$this->lock->lockMayBeAvailable($lock_id)) {
      $this->lock->wait($lock_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);

    $query = $request->request->all();
    
    if(empty($query['x_po_num'])) {
      return new Response('Missing purchase order number query parameter.', 400);
    }
    
    //TODO: Validate reponse using x_SHA1_hash
    //$response_key = 'lTPGLr4089JdXDsYVQwB';
    //$query['x_SHA1_Hash'];
    //$hash = $response_key.$query['x_login'].$query['x_trans_id'].$query['x_amount'].'.00';
    //hash_hmac('sha1', $hash, true);

    // Check for response code
    if(!empty($query['x_response_code'])) {
      if($query['x_response_code'] == static::EXACT_APPROVED) {
        $lock_id = 'commerce_saferpay_process_payment_'.$query['x_po_num'];

        if ($this->lock->acquire($lock_id)) {
          $order = $this->entityTypeManager->getStorage('commerce_order')->load($query['x_po_num']);
          
          if(!$order instanceof OrderInterface) {
            return new Response('Invalid order id.', 400);
          }

          // Common response processing for both redirect back and async notification.
          $payment = $this->processPayment($order, $request);

          if(!$payment) {
            $this->lock->release($lock_id);
            return new Response('Error while processing payment.', 400);
          }
        }

        //OK WAIT!
        // We have to go back to the webform product handler instead
        
        $response = new RedirectResponse('/checkout/'. $order->id() .'/payment/return', 302);
        $response->send();
        return;
      }
    }

    return new Response('Error while processing payment.', 400);

    // 1 for “Approved”
    // 2 for “Transaction was processed, but was not approved”
    // 3 for “Transaction was not processed due to an error”
    //  - Typically happens in the case of a Non-Funded Interac Transaction or a cancelled Paypal Transaction

    // Let's also update payment state here - it's safer doing it from received
    // asynchronous notification rather than from the redirect back from the
    // off-site redirect.
    //$state = $request->query->get('STATUS') == PaymentResponse::STATUS_AUTHORISED ? 'authorization' : 'completed';
    //$payment->set('state', $state);
    //$payment->save();

    //$response = new RedirectResponse('/', 302);
    //$response->send();
  }

  protected function processPayment(OrderInterface $order, Request $request) {
    //TODO: Check if the payment has been processed.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    // Check if the payment has been processed.
    $payments = $payment_storage->getQuery()
      ->condition('payment_gateway', $this->parentEntity->id())
      ->condition('order_id', $order->id())
      ->execute();

    // Exit if the payment has been processed.
    if (count($payments) > 0) {
      \Drupal::logger('commerce_exact')
        ->debug('Ignoring attempt to pay the already paid order %order_id.', [
          '%order_id' => $order->id(),
        ]);
      return FALSE;
    }

    $order_data = $order->getData('commerce_saferpay');
    
    $payment_values = [
      'state' => 'completed',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $request->request->get('x_trans_id'),
      'remote_state' => $request->request->get('x_response_code'),
      'authorized' => $this->time->getRequestTime(),
    ];

    $payment = $payment_storage->create($payment_values);
    $payment->save();

    return $payment;
  }

  /**
   * Set the lock service.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   *
   * @return Exact
   *   This object.
   */
  public function setLock(LockBackendInterface $lock) {
    $this->lock = $lock;
    return $this;
  }

  public function getTax(OrderInterface $order) {
    $amount = 0;
    $labels = [];
    
    foreach ($order->collectAdjustments() as $adjustment) {
      if ($adjustment->getType() !== 'tax') {
        continue;
      }
      $amount += $adjustment->getAmount()->getNumber();
      $labels[] = $adjustment->getLabel();
    }
    
    // Determine whether multiple tax types are present.
    $labels = array_unique($labels);
    if (empty($labels)) {
      $name = '';
      $description = '';
    }
    elseif (count($labels) > 1) {
      $name = 'Multiple Tax Types';
      $description = implode(', ', $labels);
    }
    else {
      $name = $labels[0];
      $description = $labels[0];
    }
    
    // Limit name, description fields to 32, 255 characters.
    $name = (strlen($name) > 31) ? substr($name, 0, 28) . '...' : $name;
    $description = (strlen($description) > 255) ? substr($description, 0, 252) . '...' : $description;
    
    // If amount is negative, do not transmit any information.
    if ($amount < 0) {
      $amount = 0;
      $name = '';
      $description = '';
    }
    
    return new Tax([
      'amount' => $amount,
      'name' => $name,
      'description' => $description,
    ]);
  }
}