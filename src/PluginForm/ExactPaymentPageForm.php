<?php

namespace Drupal\commerce_exact\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExactPaymentPageForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * Production api url.
   */
  const API_URL_LIVE = 'https://checkout.e-xact.com/payment';

  /**
   * Test api url.
   */
  const API_URL_TEST = 'https://rpm.demo.e-xact.com/payment';

  /**
   * The log service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * ExactPaymentPageForm constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->logger = $logger_factory->get('commerce_exact');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // Prevent orders that were already payed to go through the process again.
    if($order->isPaid()) {
      $this->logger->error('Order %order_id was already payed, no need to get back to the redirect form.', ['%order_id' => $order->id()]);
      throw new \UnexpectedValueException('Attempting to pay an already paid order.');
    }

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Set redirects
    $redirect_url = $configuration['mode'] === 'live' ? static::API_URL_LIVE : static::API_URL_TEST;
    $redirect_method = static::REDIRECT_POST;

    // Possibly need to provide the array of items
    $data = [
      'x_login' => $configuration['paymentpage_id'],
      'x_fp_sequence' => rand(1, 1000),
      'x_fp_timestamp' => time(),
      'x_amount' => $order->getTotalPrice()->getNumber(),
      'x_currency_code' => $order->getTotalPrice()->getCurrencyCode()
    ];

    // Concatinate data to create hash for transaction validation
    $hash = implode('^', $data);
    $data['x_fp_hash'] = hash_hmac('sha1', $hash, $configuration['transaction_key']);
    
    // Get order items and prepare line items (whether it's one or many)
    foreach ($order->getItems() as $key => $order_item) {
      // Prepare line items
      $order_item_data = array(
        $order_item->getQuantity(),
        $order_item->getTitle(),
        $order_item->getTitle(),
        $order_item->getQuantity(),
        // Round to a 2 decimal places
        \Drupal::service('commerce_price.rounder')->round($order_item->getUnitPrice())->getNumber(),
        'YES' 
      );

      $data['x_line_item_'.$key] = implode('<|>', $order_item_data);
    }

    // Add tax
    $tax = $payment_gateway_plugin->getTax($order);
    $data['x_tax'] = $tax->amount;

    // Add other required fields
    $data['x_show_form'] = 'PAYMENT_FORM';
    $data['x_receipt_link_url'] = $form['#return_url'];
    $data['x_po_num'] = $order->id();
    $data['x_email'] = $order->getEmail();

    // Map other customer fields
    //x_cust_id
    //x_invoice_num
    //x_customer_tax_id
    //x_customer_ip

    //TODO: Map other billing fields
    //x_first_name
    //x_last_name
    //x_company
    //x_address
    //x_city
    //x_state
    //x_zip
    //x_country
    //x_phone
    //x_fax

    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);

    // Add exact javascript before it redirects
    array_unshift($form['#attached']['library'], 'commerce_exact/redirect');

    return $form;
  }

  /**
   * Prepares the complete form for a POST redirect.
   *
   * Sets the form #action, adds a class for the JS to target.
   * Workaround for buildConfigurationForm() not receiving $complete_form.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed form element.
   */
  public static function processRedirectForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    $form = parent::processRedirectForm($form, $form_state, $complete_form);
    
    // Overwrite the receipt link url from webform product module
    //TODO: How to integrate better?
    if(!empty($complete_form['payment_process']['offsite_payment']['#return_url'])) {
      $form['x_receipt_link_url'] = [
        '#type' => 'hidden',
        '#value' => $complete_form['payment_process']['offsite_payment']['#return_url'],
        '#parents' => ['x_receipt_link_url'],
      ];
    }

    return $form;
  }
}