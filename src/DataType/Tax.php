<?php

namespace Drupal\commerce_authnet\DataTypes;

use CommerceGuys\AuthNet\DataTypes\BaseDataType;

class Tax extends BaseDataType {

    protected $propertyMap = [
        'amount',
        'name',
        'description',
    ];
}